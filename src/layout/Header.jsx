import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const Header = () => {
  const handleLogout = () => {
    localStorage.removeItem("username");
    localStorage.removeItem("password");

    location.href = "/login";
  };

  return (
    <div className="d-flex align-items-center justify-content-between mb-5">
      <Link to="/">
        <h1 className="mb-3 text-dark">Crud App</h1>
      </Link>

      <div className="d-flex align-items-center gap-3">
        <Link to="/api-page">API Page</Link>
        <Link to="/parent-to-child">Parent To Child</Link>
        <Button variant="danger" onClick={handleLogout}>
          Logout
        </Button>
      </div>
    </div>
  );
};

export default Header;
