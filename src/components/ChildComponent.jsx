const ChildComponent = ({ message, headingProp }) => {
  const childProp = (e) => {
    headingProp(e.target.value);
  };

  return (
    <>
      <div className="my-3">{message}</div>
      <input type="text" onChange={childProp} placeholder="Type Here" />
    </>
  );
};

export default ChildComponent;
