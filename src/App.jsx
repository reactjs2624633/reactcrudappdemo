import { BrowserRouter, Route, Routes } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";

import Home from "./pages/Home";
import Edit from "./pages/Edit";
import Login from "./pages/Login";
import AddNew from "./pages/AddNew";
import ApiPage from "./pages/ApiPage";
import Layout from "./layout/Layout";
import ProtectedRoute from "./utils/ProtectedRoute";
import ParentToChild from "./pages/ParentToChild";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <Layout />
            </ProtectedRoute>
          }
        >
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/add-new" element={<AddNew />} />
          <Route path="/edit/:id" element={<Edit />} />
          <Route path="/api-page" element={<ApiPage />} />
          <Route path="/parent-to-child" element={<ParentToChild />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
