import Login from "../pages/Login";

const ProtectedRoute = ({ children }) => {
  const username = JSON.parse(localStorage.getItem("username"));
  const password = JSON.parse(localStorage.getItem("password"));

  if (!username && !password) {
    return <Login />;
  }
  return <>{children}</>;
};

export default ProtectedRoute;
