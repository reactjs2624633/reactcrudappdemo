import { useState } from "react";
import ChildComponent from "../components/ChildComponent";

const ParentToChild = () => {
  const message = "This is from parent component";
  const [changeHeading, setChangeHeading] = useState("");

  const headingChange = (heading) => {
    setChangeHeading(heading);
  };

  return (
    <>
      <h1>Parent Component</h1>
      <ChildComponent message={message} headingProp={headingChange} />
      <div className="d-flex align-items-center gap-3 mt-3">
        <h4>Text From Child Component: </h4>
        <p>{changeHeading}</p>
      </div>
    </>
  );
};

export default ParentToChild;
