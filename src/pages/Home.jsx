import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

const Home = () => {
  const [userData, setUserData] = useState([]);

  useEffect(() => {
    const dataFromStorage = localStorage.getItem("userData");
    if (dataFromStorage) {
      const parsedData = JSON.parse(dataFromStorage);
      setUserData(parsedData);
    }
  }, []);

  const handleDelete = (userId) => {
    const updatedUserData = userData.filter((user) => user.id !== userId);
    setUserData(updatedUserData);
    localStorage.setItem("userData", JSON.stringify(updatedUserData));
  };

  return (
    <div>
      <Link to="/add-new">
        <Button variant="primary">+ Add New</Button>
      </Link>

      {/* Table */}
      <Table striped bordered hover className="mt-3">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>City</th>
            <th>Phone</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {userData.map((user, index) => (
            <tr key={user.id}>
              <td>{index + 1}</td>
              <td>{user.username}</td>
              <td>{user.email}</td>
              <td>{user.city}</td>
              <td>{user.phone}</td>
              <td className="d-flex gap-2">
                <Link to={`/edit/${user.id}`}>
                  <Button variant="secondary">Edit</Button>
                </Link>
                <Button variant="danger" onClick={() => handleDelete(user.id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Home;
