import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchDataAction } from "../redux/ApiSlice";
import { Table } from "react-bootstrap";
import DataTable from "react-data-table-component";

const ApiPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchDataAction());
  }, [dispatch]);

  const { datas, error, loading } = useSelector((state) => state?.datas);

  const columns = [
    {
      name: "#",
      selector: (row) => row.id,
      width: "50px",
    },

    {
      name: "API",
      selector: (row) => row.api,
      width: "120px",
    },

    {
      name: "Description",
      selector: (row) => row.desc,
      width: "420px",
    },

    {
      name: "Auth",
      selector: (row) => row.auth,
      width: "120px",
    },

    {
      name: "Cors",
      selector: (row) => row.cors,
      width: "80px",
    },

    {
      name: "Link",
      selector: (row) => row.link,
      width: "400px",
    },

    {
      name: "Category",
      selector: (row) => row.category,
    },
  ];

  const data = datas?.entries?.map((entry, i) => ({
    id: i + 1,
    api: entry?.API,
    desc: entry?.Description,
    auth: entry.Auth,
    cors: entry.Cors,
    link: entry.Link,
    category: entry.Category,
  }));

  return (
    <DataTable
      columns={columns}
      data={data}
      pagination
      customStyles={{
        headRow: {
          style: {
            fontSize: "18px",
          },
        },
      }}
    ></DataTable>
  );
};

export default ApiPage;
