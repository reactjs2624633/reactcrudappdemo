import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const AddNew = () => {
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    city: "",
    phone: "",
  });

  const handleChange = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const newUserData = {
      id: Date.now(),
      ...formData,
    };

    const existingData = JSON.parse(localStorage.getItem("userData")) || [];

    existingData.push(newUserData);

    localStorage.setItem("userData", JSON.stringify(existingData));

    setFormData({
      username: "",
      email: "",
      city: "",
      phone: "",
    });
  };

  return (
    <>
      <Link to="/">Back</Link>

      <form className="d-flex flex-column gap-3 mt-3" onSubmit={handleSubmit}>
        <div className="fields">
          <label htmlFor="username">Name</label>
          <input
            type="text"
            id="username"
            name="username"
            value={formData.username}
            onChange={handleChange}
            required
          />
        </div>

        <div className="fields">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
            required
          />
        </div>

        <div className="fields">
          <label htmlFor="city">City</label>
          <input
            type="text"
            id="city"
            name="city"
            value={formData.city}
            onChange={handleChange}
            required
          />
        </div>

        <div className="fields">
          <label htmlFor="phone">Mobile</label>
          <input
            type="text"
            id="phone"
            name="phone"
            value={formData.phone}
            onChange={handleChange}
            required
          />
        </div>

        <div>
          <Button variant="info" className="px-5 py-2" type="submit">
            Submit
          </Button>
        </div>
      </form>
    </>
  );
};

export default AddNew;
