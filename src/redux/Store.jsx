import { configureStore } from "@reduxjs/toolkit";
import dataReducer from "./ApiSlice";

const store = configureStore({
  reducer: {
    datas: dataReducer,
  },
});

export default store;
