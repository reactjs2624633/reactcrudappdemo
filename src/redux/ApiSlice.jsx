import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

// Initial State
const initialState = {
  datas: {},
  loading: false,
  error: null,
};

// Fetch All Data
export const fetchDataAction = createAsyncThunk(
  "datas/fetch-all",
  async (payload, { rejectWithValue }) => {
    try {
      const { data } = await axios.get("https://api.publicapis.org/entries");
      return data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const dataSlices = createSlice({
  name: "api",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchDataAction.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchDataAction.fulfilled, (state, action) => {
      state.loading = false;
      state.datas = action.payload;
    });
    builder.addCase(fetchDataAction.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
  },
});

const dataReducer = dataSlices.reducer;

export default dataReducer;
